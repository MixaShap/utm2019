import json

from flask import Flask, request

from accessories.main import get_accessories
from dijkstra.dijkstra import Dijkstra

app = Flask(__name__)
dexter = Dijkstra(path_to_df="../../data/")


def dijkstra_solve(start: int, finish: int):
    dexter.parse_to_dijkstra()
    dexter.solve(start)
    return dexter.way_finder(finish)


@app.route('/', methods=['GET'])
def server_start():
    start = request.args.get("start")
    finish = request.args.get("finish", default='104')
    gas = request.args.get("gas")
    print(start, finish)
    if gas is None:
        return "You should send me nex data: start, finish (optional), " \
               "gas<br>I can't work without it"
    if start is None:
        return "No get params<br>Usage:<br> /?start=0&gas=100<br> " \
               "/update?snow={\"elements:\" [{\"way_id\":123, \"snow_lvl\":456}]}"
    if start == finish:
        ways_ids = dexter.smelter(start_way=start)
        finish = dexter.select_most_likely(ways_ids)
        print(start, finish)
        ret_val1 = dexter.get_dist(int(start), int(finish))
        new_data1 = {'elements': [{'way_id': way_id, 'snow_lvl': 0.001} for way_id in ret_val1]}
        dexter.renew_weights(new_data=new_data1)
        ret_val2 = dexter.get_dist(int(finish), int(start))
        new_data2 = {'elements': [{'way_id': way_id, 'snow_lvl': 0.001} for way_id in ret_val2]}
        dexter.renew_weights(new_data=new_data2)
        return ', '.join([str(val) for val in ret_val1 + ret_val2[1:]])

    else:
        print(f"get data for route : start - {start}, finish: {finish}")
        start = int(start)
        finish = int(finish)
        gas = int(gas)
        if gas < 35:
            finish = 36    # gas station
        print("STARTED")
        ret_val = dexter.get_dist(start, finish)
        new_data = {'elements': [{'way_id': way_id, 'snow_lvl': 0.001} for way_id in ret_val]}
        dexter.renew_weights(new_data=new_data)
        return ', '.join([str(val) for val in ret_val])


@app.route('/update', methods=['GET'])
def data_update():
    snow_data = request.args.get("snow")
    if snow_data is None:
        return 'No get params<br>Usage: /?snow={\"elements:\" [{\"way_id\":123, \"snow_lvl\":456}]}'
    else:
        snow = json.loads(snow_data)
        snow_dict = {'elements': [{'way_id': way['way_id'], 'snow_lvl': way['snow_lvl']}
                                  for way in snow['elements']]}
        dexter.renew_weights(snow_dict)
        return "OK"


@app.route('/weather', methods=['GET'])
def accessories():
    weather_data = request.args.get("weather")
    if weather_data is None:
        return 'No get params<br>Usage: weather/?weather=' \
               '{\"elements\": [{\"weather_type\":\"snow\", \"amount\":70}, " \
                "{\"weather_type\":\"nude_ice\", \"amount\":30}]}'
    else:
        weather = json.loads(weather_data)
        return get_accessories(weather)


app.run(debug=True, host='10.19.253.231', port=5000)
