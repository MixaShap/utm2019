import math
from typing import Dict, List


def get_accessories(weather: Dict[str, List[Dict[str, float]]]):
    r"""Разбор строки погоды
        Получает на вход тип погоды
            и степень осадков в соответствующей форме:
        snow: высота в мм
        nude_ice: 0 если температура за сутки не переходила через 0
            иначе дельту температуры
    """
    width = 3
    length = 306479.69
    elements = weather['elements']
    accessories = dict()
    for element in elements:
        weather_type = element.get('weather_type')
        if weather_type == 'snow':
            # KAMAZ 6522
            # izmaylovo 306479.69 m of roads     5 mm
            # 4597.185 m^3 -> coef  -> 15 m^3 one car -> 15 cars
            accessories['ladle'] = math.ceil(0.001 * element['amount'] * width * length / 300)
        elif element['weather_type'] == 'nude_ice':
            # Контейнер для реагентов составляет 1000л
            if element['amount'] > 0:
                accessories['reagent'] = width * length / (1000 * 12)
    return accessories


'''
strr = "{\"elements\": [{\"weather_type\":\"snow\", \"critical\":70}, " \
       "{\"weather_type\":\"nude_ice\", \"critical\":30}]}"

snow = json.loads(strr)
weather_dict = {'elements': [{'weather_type': way['weather_type'],
                              'critical': way['critical']} for way in snow['elements']]}
print(get_accessories(weather_dict))
'''
