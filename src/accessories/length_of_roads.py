from mapapi import mapapi

road_json = mapapi.load_json('../../data/final.json')
total_ways = 0
for way in road_json['elements']:
    total_ways += way['distance']

print(total_ways)   # 306479.69
