from src.dijkstra.dijkstra import Dijkstra

_ = float('inf')
#                0  1  2  3  4  5  6  7
#                a  b  c  d  e  f  g  h
weightMatrix = [[0, 2, 1, 3, 9, 4, _, _],  # a 0
                [_, 0, 4, _, 3, _, _, _],  # b 1
                [_, _, 0, 8, _, _, _, _],  # c 2
                [_, _, _, 0, 7, _, _, _],  # d 3
                [_, _, _, _, 0, 5, _, _],  # e 4
                [_, _, 2, _, _, 0, 2, 2],  # f 5
                [_, _, _, _, _, 1, 0, 6],  # g 6
                [_, _, _, _, _, 9, 8, 0]]  # h 7

dexterMatrix = Dijkstra()
dexterMatrix.solve(0, 7, weightMatrix)
arr = [0] * 8
for i in range(8):
    arr[i] = i
print(weightMatrix[0][1])
print(arr)
print(dexterMatrix.weight)
way = 7
while way > -1:
    print(way, '->', end='')
    way = dexterMatrix.way[way]
print()
print(dexterMatrix.way)
