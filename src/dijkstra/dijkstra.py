# -*- coding: utf-8 -*-
import logging
from typing import List, Optional, Dict
import numpy as np

from analytics.mk_source_dataframe import load_dfs

LOGGER = logging.getLogger(__name__)


class Dijkstra:
    r"""Класс для расчета путей"""
    def __init__(self, path_to_df: str):
        """Конструктор"""
        self.weights = []
        self.ways = []
        dfs = load_dfs(path_to_df)
        self.all_points = dfs['all_points']
        self.all_vertexes_df = dfs['all_vertexes_df']
        self.dijkstra_model = dict()
        self.matrix: Optional[np.ndarray] = None
        self.weight = []

    def parse_to_dijkstra(self):
        r"""Метод для расчета матрицы связанности"""
        dijkstra_model = dict()  # dict of ways params
        counter = 0  # id of road in dijkstraModel
        infinity = np.inf  # infinity weight

        for ind, ldf in self.all_points.groupby('id_way'):
            start_point_id = ldf.iloc[0]['id']
            end_point_id = ldf.iloc[-1]['id']
            weight = self.all_vertexes_df.loc[ind, 'weight']
            dijkstra_model[counter] = {'startID': start_point_id, 'endID': end_point_id, 'weight': weight}
            counter += 1

        size = len(dijkstra_model)
        weight_matrix = [[infinity for x in range(0, size)] for x in range(0, size)]
        for i in range(0, size):
            weight_matrix[i][i] = 0

        # TODO check if this endOfNode starts other node
        for x in range(0, size):
            # curEnd = dijkstraModel[x]['endID']
            for y in range(x + 1, size):  # go in one line (whatever, X or Y)
                cond = ((dijkstra_model[y]['startID'] == dijkstra_model[x]['endID']) or
                        (dijkstra_model[y]['endID'] == dijkstra_model[x]['startID']) or
                        (dijkstra_model[y]['endID'] == dijkstra_model[x]['endID']) or
                        (dijkstra_model[y]['startID'] == dijkstra_model[x]['startID']))
                if cond:
                    weight_matrix[x][y] = dijkstra_model[x]['weight'] + dijkstra_model[x]['weight']
                    weight_matrix[y][x] = dijkstra_model[x]['weight'] + dijkstra_model[x]['weight']
        self.dijkstra_model = dijkstra_model
        self.matrix = np.array(weight_matrix)

    def solve(self, start_vertex_num):
        r"""Метод для расчета матрицы связанности"""
        num_of_vertex = len(self.matrix)
        valid = [True] * num_of_vertex
        weights = [1000000] * num_of_vertex
        weights[start_vertex_num] = 0
        self.ways = [-1] * num_of_vertex  # number of steps
        for i in range(num_of_vertex):
            min_weight = 1000001
            id_min_weight = -1
            for i in range(num_of_vertex):
                if valid[i] and weights[i] < min_weight:
                    min_weight = weights[i]
                    id_min_weight = i
            for i in range(num_of_vertex):
                if weights[id_min_weight] + self.matrix[id_min_weight][i] < weights[i]:
                    weights[i] = weights[id_min_weight] + self.matrix[id_min_weight][i]
                    self.ways[i] = id_min_weight
            valid[id_min_weight] = False
        self.weights = weights

    def way_finder(self, ender: int):
        r"""Метод для поиска одного пути в ноду с индексом ender"""
        way = []
        ender_weight = ender
        while ender > -1:
            way.append(ender)
            ender = self.ways[ender]
        if len(way) > 1:
            return {'ender_weight': self.weights[ender_weight], 'way': way}

    def get_dist(self, start_node: int, end_node: int) -> List[str]:
        r"""Метод для получения оптимального пути с действующими весами"""
        LOGGER.info(f"start calc matrix for route between {start_node, end_node}")
        self.parse_to_dijkstra()

        self.solve(start_vertex_num=start_node)
        target_way = self.way_finder(ender=end_node)['way'][::-1]
        # target_way = [str(el) for el in target_way]
        return target_way

    def renew_weights(self, new_data: Dict[str, List[Dict[str, float]]]):
        r"""Метод для обновления весов у ребер графа"""
        elements = new_data['elements']
        for element in elements:
            for k in element:
                if k == 'way_id':
                    continue
                self.all_vertexes_df.loc[element['way_id'], k] = element[k]
        self.all_vertexes_df['weight'] = 1 / (self.all_vertexes_df['snow_lvl'] *
                                              self.all_vertexes_df['distance'])
        pass

    def smelter(self, start_way, step_count: int = 40):
        last_len = 0
        ways_ids = [start_way]
        while len(ways_ids) < step_count:
            nodes = self.all_points[self.all_points['id_way'].isin(ways_ids)].id.unique()
            ways_ids = self.all_points[self.all_points['id'].isin(nodes)].id_way.unique()
            if last_len == len(ways_ids):
                break
            last_len = len(ways_ids)
        return ways_ids

    def select_most_likely(self, ways_ids):
        return self.all_vertexes_df.loc[ways_ids, 'weight'].idxmax()



