from src.mapapi import mapapi
from src.dijkstra.dijkstra import Dijkstra
import numpy as np

dijkstra_values = {}


def parseJSONtoDijkstra(jsonData):
    # dijkstraModel = {'way': {'startID': 3, 'endID': 2, 'weight': 4}}
    dijkstraModel = {}       # dict of ways params
    counter = 0              # id of road in dijkstraModel
    infinity = np.inf  # infinity weight

    for way in (jsonData["elements"]):      # one way points
        startPointID = way["nodes"][0]["id"]
        endPointID = way["nodes"][-1]["id"]
        weight = way["distance"]

        dijkstraModel[counter] = {'startID': startPointID, 'endID': endPointID, 'weight': weight}
        counter += 1
    # print("LOL")
    # print(counter)

    size = len(dijkstraModel)
    weightMatrix = [[infinity for x in range(0, size)] for x in range(0, size)]
    for i in range(0, size):
        weightMatrix[i][i] = 0
    '''
    # INFO debug output
    for i in range(0, 5):
        for point in range(0, 5):
            print(weightMatrix[i][point], '  ', end='')
        print()
    '''

    # TODO check if this endOfNode starts other node
    for x in range(0, size):
        # curEnd = dijkstraModel[x]['endID']
        for y in range(x + 1, size):    # go in one line (whatever, X or Y)
            abs_number = x   # transform X,Y to id number #TODO edit after receive normal data
            if dijkstraModel[y]['startID'] == dijkstraModel[abs_number]['endID']:
                weightMatrix[x][y] = dijkstraModel[abs_number]['weight'] + dijkstraModel[x]['weight']
                weightMatrix[y][x] = dijkstraModel[abs_number]['weight'] + dijkstraModel[x]['weight']

    '''
    print("===========ANSWER==========")
    # INFO debug output
    for i in range(0, 5):
        for point in range(0, 5):
            print(weightMatrix[i][point], '  ', end='')
        print()
    '''

    # print(weightMatrix)
    global dijkstra_values
    dijkstra_values = dijkstraModel
    return weightMatrix


def way_finder(ender: int, dijkstra: Dijkstra):
    way = []
    ender_weight = ender
    while ender > -1:
        # print(ender, '->', end='')
        way.append(ender)
        ender = dijkstra.way[ender]
    '''
    if np.isinf(dijkstra.weight[ender_weight]):
        print("!!! weight = ", dijkstra.weight[ender_weight])
    else:
        print()
    '''
    if len(way) > 1:
        print(way, end=" ")
        print("weight = ", dijkstra.weight[ender_weight])
    # print(dijkstraSolver.way)



print("program started")
MAP_SRC = "../data/moscow/final.json"
# MAP_SRC = "../data/moscow/lite-test.json"
curr_point = (55.806559, 37.793249)         # IZMAYLOVO center point
data = mapapi.load_json(MAP_SRC)
# data = mapapi.map_cut(curr_point, data)     # +radius or default
max_count = 6922
dijkstra = Dijkstra()
dijkstra.solve(0, parseJSONtoDijkstra(data))    # 0 - start point
print("Finding...")
for start in range(1, max_count):               # Where you can go from 0?
    way_finder(start, dijkstra)
'''
for start in range(0, max_count):
    for end in range(start+1, max_count):
        way_finder(start, end, )
'''
