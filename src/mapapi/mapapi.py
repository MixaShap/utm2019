import json
from math import sqrt
from copy import deepcopy
from random import randint

from src.mapapi import merge as mrg
from src.mapapi import refactor as rf
from src.mapapi import distancecalc as dc


def get_list_of_cross(data):
    """
       Создает список точек на перекрестах
       :param data: JSON объект
       :return: список точек на перекрестках
    """
    return rf.get_list_of_cross(data)


def merge(nodes_json, ways_json):
    """
     Сливает JSON объект дорог с JSON объектом точек в общий объект
    :param nodes_json: JSON объект точек
    :param ways_json: JSON объект путей
    :return: слитый объект
    """
    return mrg.merge(nodes_json, ways_json)


def load_json(json_path):
    """
    Загружает JSON объект
    :param json_path: Путь к JSON объекту
    :return: JSON объект
    """
    f = open(json_path)
    data = json.load(f)
    return data


def save(json_obj, filename):
    """
    Сохраняет  JSON объект
    :param json_obj: JSON объект
    :param filename: Путь к объекту
    """
    with open(filename, 'w') as f:
        json.dump(json_obj, f, indent=4)


def convert_gps(json_obj, r=2500):
    """
    Добавляет параментры x, y для отрисовки изображения
    :param json_obj: JSON объект
    :param r: коэффициент умноэения широты, долготы
    :return: JSON объект
    """
    tmp_json_obj = deepcopy(json_obj)
    i = 0

    for way in tmp_json_obj["elements"]:
        for node in way["nodes"]:
            pass
            x = round(r * node["lon"])
            y = round(r * node["lat"])
            node["x"] = x
            node["y"] = y
        i += 1
    return tmp_json_obj


def get_max_coordinates(json_obj):
    """
    Находит максимальную x и y координаты
    :param json_obj: JSON объект
    :return: максимальную x и y координаты
    """
    max_x = json_obj["elements"][0]["nodes"][0]["x"]
    max_y = json_obj["elements"][0]["nodes"][0]["y"]
    for way in json_obj["elements"]:
        for node in way["nodes"]:
            if node["x"] > max_x:
                max_x = node["x"]
            if node["y"] > max_y:
                max_y = node["y"]

    return max_x, max_y


def get_min_coordinates(json_obj):
    """
    Находит минимальную x и y координаты
    :param json_obj: JSON объект
    :return: минимальную x и y координаты
    """
    max_x = json_obj["elements"][0]["nodes"][0]["lat"]
    max_y = json_obj["elements"][0]["nodes"][0]["lon"]
    for way in json_obj["elements"]:
        for node in way["nodes"]:
            if node["x"] < max_x:
                max_x = node["x"]
            if node["y"] < max_y:
                max_y = node["y"]

    return max_x, max_y


def map_cut(point, json_obj, radius=0.04, id_flag=False):
    """
    Функция генерирует новый JSON объект, состоящий из точек в заданном радиусе
    :param point: Точка, в радиусе которой нужно сгенерировать новый JSON объект
    :param json_obj: исходный JSON объект
    :param radius: радиус
    :param id_flag: флаг, добавляющий в новый объект id путей
    :return: новый JSON объект
    """
    new_json_obj = {"elements": []}

    for way_index in range(len(json_obj["elements"])):
        way = {"type": "way", "nodes": []}
        if id_flag:
            way = {"type": "way", "nodes": [], "id": json_obj["elements"][way_index]["id"]}
        for node_index in range(len(json_obj["elements"][way_index]["nodes"])):
            lat = json_obj["elements"][way_index]["nodes"][node_index]["lat"]
            lon = json_obj["elements"][way_index]["nodes"][node_index]["lon"]

            if count_distance(point, (lat, lon)) <= radius:
                way["nodes"].append(json_obj["elements"][way_index]["nodes"][node_index])
        if len(way["nodes"]) != 0:
            new_json_obj["elements"].append(way)
    return new_json_obj


def count_distance(p1, p2):
    """
    Подсчитывает расстояние между двумя точками
    :param p1: точка 1
    :param p2: точка 2
    :return:
    """
    return sqrt((p2[0] - p1[0])**2+(p2[1] - p1[1])**2)


def get_random_node(json_obj):
    """
    Возвращает случайную точку на карте
    :param json_obj: JSON объект
    :return:
    """
    r1 = randint(0, len(json_obj["elements"]) - 1)
    r2 = randint(0, len(json_obj["elements"]) - 1)
    return json_obj["elements"][r1]["nodes"][0], json_obj["elements"][r2]["nodes"][0]


def convert_for_dijkstra(json_obj):
    """
    Разбивает список точек на графы, учитывая перекрестки
    :param json_obj: JSON объект
    :return: новый JSON объект
    """

    tmp_json_obj = deepcopy(json_obj)

    cross_points = get_list_of_cross(tmp_json_obj)

    for ways in (tmp_json_obj["elements"]):
        for node in ways["nodes"]:
            if node in cross_points:
                node["type"] = 'node_finish'

    result = {"elements": []}
    for way_index in range(len(tmp_json_obj["elements"])):
        way = {"type": "way", "nodes": []}
        for node_index in range(len(tmp_json_obj["elements"][way_index]["nodes"])):
            node = tmp_json_obj["elements"][way_index]["nodes"][node_index]
            if node["type"] == 'node' or node_index == len(tmp_json_obj["elements"][way_index]["nodes"]) - 1:
                way["nodes"].append(deepcopy(node))
            else:
                way["nodes"].append(deepcopy(node))
                result["elements"].append(way)
                way = {"type": "way", "nodes": [deepcopy(node)]}
                way["nodes"].append(deepcopy(node))
        result["elements"].append(way)
    return result


def set_way_id(json_obj):
    """
    Устанавливает id путей, помечает стартовую и конечную точки путя
    :param json_obj: JSON объект
    :return: новый JSON объект
    """
    tmp_json_obj = deepcopy(json_obj)
    for way_index in range(len(tmp_json_obj["elements"])):
        tmp_json_obj["elements"][way_index]["id"] = way_index
        for node_index in range(len(tmp_json_obj["elements"][way_index]["nodes"])):
            if node_index == 0:
                tmp_json_obj["elements"][way_index]["nodes"][node_index]["type"] = "node_start"
            elif node_index == len(tmp_json_obj["elements"][way_index]["nodes"]) - 1:
                tmp_json_obj["elements"][way_index]["nodes"][node_index]["type"] = "node_finish"
            else:
                tmp_json_obj["elements"][way_index]["nodes"][node_index]["type"] = "node"

    return tmp_json_obj


def set_distance(json_obj):
    """
    Установка расстояний между вершинами графов
    :param json_obj: JSON объект
    :return: Новый JSON объект
    """
    for way_index in range(len(json_obj["elements"])):
        distance = 0
        for node_index in range(len(json_obj["elements"][way_index]["nodes"])-1):
            distance += dc.earth_distance_in_meters(json_obj["elements"][way_index]["nodes"][node_index]["lat"],
                                                    json_obj["elements"][way_index]["nodes"][node_index]["lon"],
                                                    json_obj["elements"][way_index]["nodes"][node_index+1]["lat"],
                                                    json_obj["elements"][way_index]["nodes"][node_index+1]["lon"])

        json_obj["elements"][way_index]["distance"] = distance
    return json_obj


def del_empty_ways(json_obj):
    result = {"elements": []}
    for way in json_obj["elements"]:
        if len(way["nodes"]) > 1:
            result["elements"].append(way)
        else:
            print("empty")
    return result
