from math import pi, sin, sqrt, cos, atan2

EARTH_RADIUS = 6371


def degrees_to_radians(degrees):
    """
    Перевод градусов в радианы
    :param degrees: градусы
    :return: радианы
    """
    return degrees * pi / 180


def earth_distance_in_meters(lat1, lon1, lat2, lon2):
    """
    Подчет расстояния от одной точки до другой
    :param lat1: Широта первой точки
    :param lon1: Долгота первой точки
    :param lat2: Широта второй точки
    :param lon2: Долгота второй точки
    :return: Расстояние в метрах
    """
    d_lat = degrees_to_radians(lat2-lat1)
    d_lon = degrees_to_radians(lon2-lon1)

    lat1 = degrees_to_radians(lat1)
    lat2 = degrees_to_radians(lat2)

    a = sin(d_lat/2) * sin(d_lat/2) + sin(d_lon/2) * sin(d_lon/2) * cos(lat1) * cos(lat2)
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    return round(EARTH_RADIUS * c * 1000, 5)
