def search_crossroads_by_node_id(node_id, json_obj):
    """
    Ищет совпадения данной точки в объекте
    :param node_id: id точки
    :param json_obj: JSON объект
    :return: словарь точки, если найдено совпадение, Null - если не найдено
    """
    count = 0
    node_result = None
    for ways_tmp in json_obj["elements"]:
        for node_tmp in ways_tmp["nodes"]:
            if node_tmp["id"] == node_id:
                node_result = node_tmp
                count += 1
    if count > 1:
        return node_result
    else:
        return None


def get_list_of_cross(json_obj):
    """
    Создает список точек на перекрестах
    :param json_obj: JSON объект
    :return: список точек на перекрестках
    """
    result_li = []
    for ways in (json_obj["elements"]):
        for node in ways["nodes"]:
            node_check = search_crossroads_by_node_id(node["id"], json_obj)
            if node_check is not None:
                result_li.append(node_check)

    return result_li
