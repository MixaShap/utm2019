
class Node:
    def __init__(self, lat, lon, node_id):
        self.lat = lat
        self.lon = lon
        self.node_id = node_id


class Way:
    node_li = []

    def __init__(self, nodes_id):
        self.nodes_id = nodes_id


def merge(nodes_json, ways_json):
    nodes_li = get_nodes_list(nodes_json)
    ways_li = get_ways_list(ways_json)
    return create_merged_json_obj(nodes_li, ways_li)


def get_nodes_list(obj):
    nodes_elements = obj["elements"]

    nodes = []
    for el in nodes_elements:
        nodes.append(Node(el["lat"], el["lon"], el["id"]))
    return nodes


def get_ways_list(obj):
    ways_elements = obj["elements"]
    ways = []
    for el in ways_elements:
        ways.append(Way(el["nodes"]))
    return ways


def search_node_by_id(nodes, node_id):
    for node in nodes:
        if node_id == node.node_id:
            return node


def create_merged_json_obj(nodes, ways):
    main_dictionary = {"elements": []}
    elements = []
    print("START MERGING")
    for i in range(len(ways) - 1):
        print(str(round(i/(len(ways) - 1), 4)*100) + "%")
        way = {"type": "way", "nodes": []}
        for element in ways[i].nodes_id:
            tmp = search_node_by_id(nodes, element)
            node = {"type": "node", "id": tmp.node_id, "lat": tmp.lat, "lon": tmp.lon}
            way["nodes"].append(node)
        elements.append(way)
    print("END MERGING")
    main_dictionary["elements"] = elements
    return main_dictionary
