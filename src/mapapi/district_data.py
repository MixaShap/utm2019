from src.mapapi import mapapi
from datetime import datetime

start_time = datetime.now()
MERGED_DATA_SRC = "../data/moscow/merged.json"
ZOOM = 10000
DISTRICT_CENTRE_POINT = (55.806559, 37.793249)

# выгружаем данные
map_data = mapapi.load_json(MERGED_DATA_SRC)

# обрезаем по радиусу района
map_data = mapapi.map_cut(DISTRICT_CENTRE_POINT, map_data)
# добавляем координаты x, y для отрисовки
map_data = mapapi.convert_gps(map_data, r=ZOOM)
# преобразуем в графы
map_data = mapapi.convert_for_dijkstra(map_data)
# удаляет пустые пути
map_data = mapapi.del_empty_ways(map_data)
# устанавливаем индексы путей
map_data = mapapi.set_way_id(map_data)
# устанавливаем расстояние между точками
map_data = mapapi.set_distance(map_data)
# сохраняем файл
mapapi.save(map_data, "final.json")

print(datetime.now()-start_time)
