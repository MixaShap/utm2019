from src.mapapi import mapapi
# TODO algorithm: take dict of ways and nodes
#  --> select normal radius for point search
#  --> compare list of nodes with list of  oil stations

radius = 0.0005
# coord                                 # way   # node
gas_station = [55.804830, 37.781781]    # 4166  # 1271026079
garage = [55.795431, 37.775225]         # 1617  # 3933673225
melter = [55.796840, 37.819434]         # 5647  # 3981494890
search_point = gas_station
data = mapapi.load_json("../../../data/final.json")
for way in data["elements"]:
    # print(way)
    # exit()
    for node in way["nodes"]:
        # print(node)
        # exit()
        if (abs(node['lat'] - search_point[0]) < radius) & (abs(node['lon'] - search_point[1]) < radius):
            print("=======FOUND=======")
            print(node["lat"], node["lon"], node["id"], way["id"])
# print(data)
