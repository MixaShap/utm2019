import json
from pathlib import Path
from typing import Optional, Dict

import pandas as pd


def mk_pandas_from_json(path_to_source: str, path_to_result: Optional[str] = None,
                        need_return_df: bool = True) -> Optional[Dict[str, pd.DataFrame]]:
    r"""
    path_to_source: 'root/data/final.json'
    path_to_result: path to result folder 'root/data/'
    """
    if path_to_result is None:
        path_to_result = '.'
    f = json.load(open(path_to_source, 'r'))
    elements = f['elements']
    all_points = pd.DataFrame()
    all_vertexes = []
    for num, el in enumerate(elements):
        id_n = el['id']
        distance = el['distance']
        l_edges = pd.DataFrame(el['nodes'])
        l_vertexes = {'id_way': id_n, 'distance': distance}
        l_edges['id_way'] = id_n
        all_points = all_points.append(l_edges)
        all_vertexes.append(l_vertexes)
    all_vertexes_df = pd.DataFrame(all_vertexes).set_index('id_way')
    all_vertexes_df['trafick_jam'] = 6  # yandex score
    all_vertexes_df['snow_lvl'] = 0.02  # meters
    all_vertexes_df['width'] = 5  # meters
    all_vertexes_df['weight'] = 1 / (all_vertexes_df['snow_lvl'] * all_vertexes_df['distance'])
    # all_vertexes_df['weight'] = 1 / ((all_vertexes_df['distance'] * all_vertexes_df['snow_lvl'] *
    #                                   all_vertexes_df['width']) / all_vertexes_df['trafick_jam'])
    # all_vertexes_df = all_vertexes_df[all_vertexes_df['distance'] > 0]
    index_cor = {external: internal for internal, external in enumerate(all_vertexes_df.index)}
    # all_points = all_points[all_points['id_way'].isin(all_vertexes_df.index.unique())]
    # all_points['id_way'] = all_points['id_way'].map(index_cor)
    # all_vertexes_df.index = all_vertexes_df.index.map(index_cor)

    if need_return_df:
        return {'all_vertexes_df': all_vertexes_df, 'all_points': all_points, 'index_cor': index_cor}
    else:
        all_vertexes_df.to_pickle(str(Path(path_to_result).joinpath('all_vertexes_df.pickle')))
        all_points.to_pickle(str(Path(path_to_result).joinpath('all_points.pickle')))


def load_dfs(path_to_pickles: str):
    all_vertexes_df = pd.read_pickle(str(Path(path_to_pickles).joinpath('all_vertexes_df.pickle')))
    all_points = pd.read_pickle(str(Path(path_to_pickles).joinpath('all_points.pickle')))
    return {'all_points': all_points, 'all_vertexes_df': all_vertexes_df}
