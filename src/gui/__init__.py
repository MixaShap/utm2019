from src.mapapi import mapapi
from src.gui import truck

from PyQt5 import QtWidgets, uic, QtGui, QtCore
import sys
import json
from random import randint
from time import sleep

BLACK_COLOR = QtCore.Qt.black
GREEN_COLOR = QtCore.Qt.green

YELLOW_COLOR = QtCore.Qt.yellow
BLUE_COLOR = QtCore.Qt.blue
RED_COLOR = QtCore.Qt.red

PEN_WIDTH = 1.0

PEN_WIDTH_BOLD = 3.0

SCENE_W = 589
SCENE_H = 449

ZOOM_STEP = 15000

MAP_SRC = "/Users/kit/PycharmProjects/UTM/src/final.json"

GAS_STATION_NODE_ID = 3933631351
HANGAR_STATION_NODE_ID = 3791677445
SMELTER_STATION_NODE_ID = 636786767

GAS_STATION_WAY_ID = 36
HANGAR_STATION_WAY_ID = 74
SMELTER_STATION_WAY_ID = 104


class MainWindow(QtWidgets.QMainWindow):

    model = QtGui.QStandardItemModel()

    zoom = 10000
    test_index = 0

    def __init__(self, parent=None, flags=QtCore.Qt.Window):
        QtWidgets.QMainWindow.__init__(self, parent, flags)
        uic.loadUi('mainwindow.ui', self)

        self.scene = QtWidgets.QGraphicsScene()
        self.zoominButton.clicked.connect(self.zoom_in)
        self.zoomoutButton.clicked.connect(self.zoom_out)
        self.moveCar.clicked.connect(self.next_step_of_simulation)

        self.map_data = mapapi.load_json(MAP_SRC)
        self.map_data = mapapi.convert_gps(self.map_data, r=self.zoom)
        self.draw_map(self.map_data)

        start_way = self.map_data["elements"][1388]
        self.truck1 = truck.Truck(start_way, self.map_data)
        self.truck1.set_route(None)

        print(self.get_way_id_from_node_id(GAS_STATION_NODE_ID))
        print(self.get_way_id_from_node_id(HANGAR_STATION_NODE_ID))
        print(self.get_way_id_from_node_id(SMELTER_STATION_NODE_ID))

    def next_step_of_simulation(self):
        self.truck1.truck_move()

        for way_id in self.truck1.current_route[self.truck1.current_way_index:]:
            way = self.get_way_from_id(way_id)
            way_li = self.get_list_of_way(way)
            for el in way_li:
                self.draw_way(el[0], el[1], color=GREEN_COLOR, width=2, doted=True)

        way = self.get_way_from_id(self.truck1.current_way_id)
        way_li = self.get_list_of_way(way)
        for el in way_li:
            self.draw_way(el[0], el[1], color=GREEN_COLOR, width=3)

    def get_node_coordinates(self, node_id):
        """
        Возвращаяет координаты точек
        :param node_id: id точки
        :return: x y координаты точки
        """
        for way in self.map_data["elements"]:
            for node in way["nodes"]:
                if node["id"] == node_id:
                    return node["x"], node["y"]

    def get_way_id_from_node_id(self, node_id):
        """
        Возвращаяет id пути для id точки
        :param node_id: id точки
        :return: id пути
        """
        for way in self.map_data["elements"]:
            for node in way["nodes"]:
                if node["id"] == node_id:
                    return way["id"]

    def get_way_from_id(self, way_id):
        """
        Возвращаяет координаты точек
        :param way_id: id точки
        :return: x y координаты точки
        """
        for way in self.map_data["elements"]:
            if way["id"] == way_id:
                return way

    def draw_point(self, point, radius=3, color=RED_COLOR):
        """
        Отрисовка точки
        :param point: x, y координаты точки
        :param radius: радиус точки
        :param color: цвет точки
        """
        x = point[0]
        y = point[1]

        pen = QtGui.QPen(color, PEN_WIDTH)
        p = QtCore.QRectF(x - radius, y - radius, radius * 2, radius * 2)

        brush = QtGui.QBrush(QtCore.Qt.SolidPattern)
        brush.setColor(color)
        self.scene.addEllipse(p, pen, brush)
        self.graphicsView.setScene(self.scene)

    def draw_way(self, pos1, pos2, color=BLACK_COLOR, width=PEN_WIDTH, doted=False):
        """
        Отрисовка отрезка
        :param pos1: Начальная точка
        :param pos2: Конечная точка
        :param color: цвет линии
        :param width: ширина линии
        :param doted: флаг пунктира
        """
        pen = QtGui.QPen(color, width)
        if doted:
            pen = QtGui.QPen(color, width, QtCore.Qt.DotLine)
        self.scene.addLine(pos1[0], pos1[1], pos2[0], pos2[1], pen)
        self.graphicsView.setScene(self.scene)

    def draw_map(self, json_obj):
        """
        Полная отрисовка карты
        :param json_obj: JSON объект
        """
        self.scene = QtWidgets.QGraphicsScene()

        for way in json_obj["elements"]:
            way_list = self.get_list_of_way(way)
            for el in way_list:
                self.draw_way(el[0], el[1])

        for way_index in range(len(json_obj["elements"])):
            for node_index in range(len(json_obj["elements"][way_index]["nodes"])):
                node = json_obj["elements"][way_index]["nodes"][node_index]
                if node_index == len(json_obj["elements"][way_index]["nodes"]) - 1:
                    if node["id"] == GAS_STATION_NODE_ID:
                        self.draw_point((node["x"], node["y"]), color=YELLOW_COLOR, radius=3)
                    elif node["id"] == HANGAR_STATION_NODE_ID:
                        self.draw_point((node["x"], node["y"]), color=BLUE_COLOR, radius=3)
                    elif node["id"] == SMELTER_STATION_NODE_ID:
                        self.draw_point((node["x"], node["y"]), color=RED_COLOR, radius=3)
                    else:
                        self.draw_point((node["x"], node["y"]), color=BLACK_COLOR, radius=1)

    def zoom_in(self):
        """
        Увеличение масштаба
        """
        self.zoom += ZOOM_STEP
        self.map_data = mapapi.convert_gps(self.map_data, r=self.zoom)
        self.draw_map(self.map_data)

    def zoom_out(self):
        """
        Уменьшение масштаба
        """
        self.zoom -= ZOOM_STEP
        self.map_data = mapapi.convert_gps(self.map_data, r=self.zoom)
        self.draw_map(self.map_data)

    @staticmethod
    def get_list_of_way(way):
        """
        Получает спиоск для отрисовки отрезков с помощью метода draw_point
        :param way: Путь
        :return: Список для отрисоквки
        """
        li = []
        for node in way["nodes"]:
            li.append((node["x"], node["y"]))

        new_li = []
        for i in range(len(li) - 1):
            new_li.append([li[i], li[i + 1]])

        return new_li


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    rc = app.exec_()
    del window
    sys.exit(rc)


if __name__ == '__main__':
    main()
