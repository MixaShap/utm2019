import urllib.request


class Truck:
    fuel = 50
    fuel_consumption_per_meter = 0.01
    speed = 4

    current_snow_volume = 0
    total_snow_volume = 100

    current_way_index = 0

    def __init__(self, current_way_id, json_obj):
        self.current_way_id = current_way_id
        self.current_route = [self.current_way_id]
        self.data = json_obj

    def set_route(self, current_way):
        """
        Составляет маршрут
        :param current_way: Текущая точка, на которой стоит машина
        :return: Список, состоящий из id путей по которым нужно пройти
        """
        response = urllib.request.urlopen('http://10.19.252.53:5000/?start=81')
        r = str(response.read())[2:].split(', ')
        print(r)

        ways_id = [81, 80, 3410, 3680, 3337, 3417, 3336, 3335, 4430, 4431, 4432, 4433, 982, 4434, 4435, 4436, 4437, 1601, 4438, 1285, 4439, 638, 4440, 641, 4441, 4442, 224, 223, 647, 4444, 4528, 4527, 4526, 4447, 4448, 4449, 3062, 3064, 3088, 3065, 3067, 3068, 3072, 3073, 2149, 3001, 2055, 3406, 3405, 3377, 3398, 3384, 4474, 317, 316, 315, 314, 313, 312, 1184, 1185, 4326, 4325, 118]

        self.current_route = ways_id
        return ways_id

    def truck_move(self):
        """
        Перемещает машину на следующуий way
        """
        if self.current_way_index != len(self.current_route) - 1:
            self.current_way_index += 1
            self.current_way_id = self.current_route[self.current_way_index]
        else:
            print("End")
            # конец маршрута
            pass

    def search_way_by_id(self, way_id):
        """
        Поиск пути с уникальным идентификатором
        :param way_id: id пути
        :return: Словарь пути с данным id
        """
        for way in self.data["elements"]:
            if way["id"] == way_id:
                return way
