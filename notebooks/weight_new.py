# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     cell_metadata_filter: all
#     notebook_metadata_filter: all,-language_info
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: urbanhack
#     language: python
#     name: urbanhack
# ---

# %%
# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

# %%
import sys
from pathlib import Path
import pandas as pd
import numpy as np
import time
import json

# %%
from matplotlib import pyplot as plt
import seaborn as sns
import plotly.graph_objects as go
from plotly.offline import plot

# %%
cure_dir = Path('..').absolute()
src_folder = [p for p in cure_dir.glob('*') if p.name == 'src'][0]
sys.path.append(str(src_folder))

# %%
sys.path.append('/Users/yuriysimonov/Documents/GitHub/urbanhack')

# %%
from analytics.mk_source_dataframe import mk_pandas_from_json
from dijkstra.dijkstra import Dijkstra
# from mapapi import mapapi

# %%
path_to_source = '/Users/yuriysimonov/Documents/GitHub/urbanhack/data/final.json'
# path_to_source = '/Users/yuriysimonov/Documents/GitHub/urbanhack/data/final_tiny.json'
path_to_result = '/Users/yuriysimonov/Documents/GitHub/urbanhack/data/'

# %%
# # %%time
# dfs = mk_pandas_from_json(path_to_source)

# %%
# %%time
Path(path_to_result).mkdir(exist_ok=True)
mk_pandas_from_json(path_to_source=path_to_source, path_to_result=path_to_result, need_return_df=False)

# %%
# dijkstra = Dijkstra(path_to_df='/Users/yuriysimonov/Documents/GitHub/urbanhack/data_test/')
dijkstra = Dijkstra(path_to_df='/Users/yuriysimonov/Documents/GitHub/urbanhack/data/')

# %%
# dijkstra.all_vertexes_df['weight'] = (dijkstra.all_vertexes_df['weight'] -
#                                       dijkstra.all_vertexes_df['weight'].min() * 1.1)

# %%
dijkstra.all_vertexes_df['weight'].agg([np.min, np.max])

# %%
dijkstra.all_vertexes_df[dijkstra.all_vertexes_df['weight'] > 100000000]

# %%
last_len = 0
ways_ids = [3061]
while len(ways_ids) < 7:
    nodes = dijkstra.all_points[dijkstra.all_points['id_way'].isin(ways_ids)].id.unique()
    ways_ids = dijkstra.all_points[dijkstra.all_points['id'].isin(nodes)].id_way.unique()
    if last_len == len(ways_ids):
        break
    last_len = len(ways_ids)

# %%
ways_ids

# %%
dijkstra.all_vertexes_df['weight'].min()

# %%
fig = [go.Scatter(x=ldf['x'], y=ldf['y'], name=way_id) for way_id, ldf in dijkstra.all_points.groupby('id_way')
       if way_id in ways_ids]
plot(fig)

# %%
dijkstra.all_points[dijkstra.all_points['id_way'].isin([4717, 2473])].id.unique()

# %%
pppp = dijkstra.all_points[dijkstra.all_points['id_way'].isin([1028, 4459])].id.unique()

# %%
dijkstra.all_vertexes_df.index.max()

# %%
# %%time
dijkstra.parse_to_dijkstra()
dijkstra.solve(start_vertex_num=1617)

# %%
rv = dijkstra.way_finder(ender=4166)

# %%
rv

# %%
new_data = {'elements': [{'way_id': way_id, 'snow_lvl': 0.001} for way_id in rv['way']]}

# %%
dijkstra.renew_weights(new_data=new_data)

# %%
# %%time
dijkstra.parse_to_dijkstra()
dijkstra.solve(start_vertex_num=4717)
rv_2 = dijkstra.way_finder(ender=2473)
new_data = {'elements': [{'way_id': way_id, 'snow_lvl': 0.001} for way_id in rv_2['way']]}

# %%
dijkstra.renew_weights(new_data=new_data)

# %%
# %%time
dijkstra.parse_to_dijkstra()
dijkstra.solve(start_vertex_num=4717)
rv_3 = dijkstra.way_finder(ender=2473)
new_data = {'elements': [{'way_id': way_id, 'snow_lvl': 0.001} for way_id in rv_3['way']]}

# %%
dijkstra.renew_weights(new_data=new_data)

# %%
in_ways =  rv['way'] +  rv_2['way'] +  rv_3['way']

# %%
fig = [go.Scatter(x=ldf['x'], y=ldf['y'], name=way_id, marker=dict(color="yellow")) 
                  for way_id, ldf in dijkstra.all_points.groupby('id_way')
       if (way_id in ways_ids) and (way_id not in in_ways)]
for cur_rv, color in zip([rv, rv_2, rv_3], ['red', 'blue', 'green']):
    for way_id in cur_rv['way']:
        ldf = dijkstra.all_points[dijkstra.all_points['id_way'] == way_id]
        fig.append(go.Scatter(x=ldf['x'], y=ldf['y'], name=way_id, marker=dict(color=color)))
plot(fig)
