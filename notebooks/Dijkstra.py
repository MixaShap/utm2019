# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     cell_metadata_filter: all
#     notebook_metadata_filter: all,-language_info
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: urbanhack
#     language: python
#     name: urbanhack
# ---

# %%
# %load_ext autoreload
# %autoreload 2

# %%
import sys
from pathlib import Path

# %%
cure_dir = Path('..').absolute()
src_folder = [p for p in cure_dir.glob('*') if p.name == 'src'][0]
sys.path.append(str(src_folder))

# %%
src_folder

# %%
sys.path.append('/Users/yuriysimonov/Documents/GitHub/urbanhack')

# %%
from dijkstra.dijkstra import Dijkstra
from mapapi import mapapi
from dijkstra.parceJSONtoDijkstra import parseJSONtoDijkstra, way_finder

# %%
import json
import numpy as np
import pandas as pd
import copy

# %%
data = mapapi.load_json('/Users/yuriysimonov/Documents/GitHub/urbanhack/data/final.json')

# %%
data['elements'][0]['nodes']

# %%
max_count = len(data['elements'])

# %%
dijkstra = Dijkstra()

# %%
rv = np.array(parseJSONtoDijkstra(data))

# %%
rv[0][rv[0] != np.inf]

# %%
# %%time
solve = dijkstra.solve(0, rv)

# %%
way_finder(1388, dijkstra)

# %%
all_vertexes_df

# %%
path_to_json = Path('/Users/yuriysimonov/Documents/GitHub/urbanhack/data/final.json')

# %%
f = json.load(open(str(path_to_json), 'r'))
elements = f['elements']

# %%
# %%time
# parse json
all_points = pd.DataFrame()
all_vertexes = []
for num, el in enumerate(elements):
    id_n = el['id']
    distance = el['distance']
    l_edges = pd.DataFrame(el['nodes'])
    l_vertexes = {'id_way': id_n, 'distance': distance}
    l_edges['id_way'] = id_n
    all_points = all_points.append(l_edges)
    all_vertexes.append(l_vertexes)
#     break
# mk DataFrame
# all_ways_df = pd.DataFrame(np.concatenate(all_ways, axis=0))

# create new column
# all_ways_df['coord'] = all_ways_df.apply(lambda x: (x['lat'], x['lon']), axis=1)

# %%
all_vertexes_df = pd.DataFrame(all_vertexes).set_index('id_way')

# %%
all_vertexes_df.loc[2, 'distance']

# %%
all_points[all_points['id_way'] == 1388]

# %%
all_vertexes_df

# %% {"collapsed": true, "jupyter": {"outputs_hidden": true}}
for ind, ldf in all_points.groupby('id_way'):
    start_point_id = ldf[ldf['type'] == 'node_start']['id']
    end_point_id = ldf[ldf['type'] == 'node_finish']['id']
    weight = all_vertexes_df.loc[ind, 'distance']

# %%
all_vertexes_df.to_pickle('all_vertexes_df.pickle')
all_points.to_pickle('all_points.pickle')

# %%
pd.read_pickle('all_vertexes_df.pickle')

# %%
all_vertexes_df[all_vertexes_df['id_n'] == 1388]['distance']

# %%

# %%

# %%
all_points[all_points['type'] != 'node'].shape[0] - \
all_points[all_points['type'] != 'node'][['lat', 'lon']].drop_duplicates().shape[0]

# %%
all_points[all_points['type'] != 'node'].shape[0]

# %%
all_points[all_points['id_n'] == 6920].to_dict()

# %%
all_points.groupby(['lat', 'lon']).nunique().sort_values('id_n', ascending=False).head(50)

# %%
all_points

# %%
all_points[all_points['lat'] == 55.8224653]

# %%
all_points[all_points['id'] == 724833369].iloc[0][['lat', 'lon']].to_dict()

# %%
all_points[['lat', 'lon']]

# %%
pd.DataFrame(all_vertexes)['id_n']

# %%
start_potin = {'lat': 55.8224653, 'lon': 37.756933}

# %%
724833369


# %%
def get_edges(start_potin_id):
    return all_points[all_points['id'] == start_potin_id]


# %%
список машин и среднее количество часов работы.

по ВАО количество жиличник измаилово

координаты базы

заправки стандартные



# %%
np.isinf(np.inf)

# %%

# %%

# %%

# %%

# %% [markdown]
# УНОМ
#
# БНСО каждые 30 секунд собирает информацию.
#
# протокол ЕГТС (что за хрень)
#
# было задание жиличнику (в случае снега частота)
#
# использовать камеры для оценки осадков
#
# оптимизация анализа отзыва (предлогать им семантический анализ отзывов?)
