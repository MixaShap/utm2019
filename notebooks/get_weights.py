# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     cell_metadata_filter: all
#     notebook_metadata_filter: all,-language_info
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: urbanhack
#     language: python
#     name: urbanhack
# ---

# %%
# %load_ext autoreload
# %autoreload 2

# %%
import sys
from pathlib import Path
import pandas as pd
import numpy as np
import time

# %%
from matplotlib import pyplot as plt

# %%
from matplotlib import pyplot as plt
# %matplotlib inline

import seaborn as sns

# %%
cure_dir = Path('..').absolute()
src_folder = [p for p in cure_dir.glob('*') if p.name == 'src'][0]
sys.path.append(str(src_folder))

# %%
sys.path.append('/Users/yuriysimonov/Documents/GitHub/urbanhack')

# %%
from analytics.mk_source_dataframe import mk_pandas_from_json
from dijkstra.dijkstra import Dijkstra
# from mapapi import mapapi

# %%
# import json
# import numpy as np
# import pandas as pd
# import copy

# %%
dijkstra = Dijkstra(path_to_df='/Users/yuriysimonov/Documents/GitHub/urbanhack/data')

# %%
# %%time
dijkstra.parse_to_dijkstra()

# %%
# %%time
dijkstra.solve(start_vertex_num=1388)

# %%
rv = dijkstra.way_finder(ender=0)

# %%
rv['way'][::-1]

# %%
dijkstra.all_vertexes_df

# %%
rv['way'][::-1]

# %%
temp_df = dijkstra.all_points.set_index('id_way').loc[rv['way'][::-1]].drop_duplicates().copy()

# %%
temp_df

# %%
temp_df

# %%
temp_df[['x', 'y', 'id']].drop_duplicates()['id'].values

# %%
xy = temp_df[['x', 'y', 'id']].drop_duplicates()

# %%
xy['x'].max(), xy['x'].min()

# %%
import json

# %%
xy

# %%
json.dumps(xy.to_dict(orient='records'))

# %%
xy['x']

# %%
i=
sns.scatterplot(x=xy['x'][:i],
                y=xy['y'][:i])
plt.xlim(xy['x'].min(), xy['x'].max())
plt.ylim(xy['y'].max(), xy['y'].min())

# %%
dijkstra.all_points[dijkstra.all_points['id_way'].isin([0, 1])]

# %%
i=2
sns.scatterplot(x=dijkstra.all_points.set_index('id_way').loc[rv['way']].['x'][:i],
                y=dijkstra.all_points.set_index('id_way').loc[rv['way']]['y'][:i])

# %%
for i in range(temp_df.shape[0]):
    sns.scatterplot(x=dijkstra.all_points.set_index('id_way').loc[rv['way']]['x'][i:],
                    y=dijkstra.all_points.set_index('id_way').loc[rv['way']]['y'][i:])
#     time.sleep(0.5)
# sns.scatterplot(x=dijkstra.all_points.set_index('id_way').loc[rv['way']]['x'][0],
#                 y=dijkstra.all_points.set_index('id_way').loc[rv['way']]['y'][0])

# %%

# %%

# %%

# %%

# %%
# ls /Users/yuriysimonov/Documents/GitHub/urbanhack/data

# %%
path_to_source = '/Users/yuriysimonov/Documents/GitHub/urbanhack/data/final.json'

# %%
dfs = mk_pandas_from_json(path_to_source=path_to_source)

# %%
dfs.keys()

# %%
all_vertexes_df = dfs['all_vertexes_df']
all_points = dfs['all_points']

# %%
# %%time
weight_matrix = parseJSONtoDijkstra(all_points=all_points, all_vertexes_df=all_vertexes_df)

# %%
{'d': 2, 'e': 4}

# %%
{2, 4, 5}

# %%
weight_matrix = np.array(weight_matrix)

# %%
dijkstra = Dijkstra()

# %%

# %%
